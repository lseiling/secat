import pandas as pd
from pathlib import Path
from data_preprocessing import *

"""
Class containing all neccessary functions to extract basic metrics from neo4j database
"""
class MetricGenerator(object):

	def __init__(self, database):
		self.database = database

	def get_total_tweets(self):
		"""
        Query to get tweet count from active graph.

        Returns:
            int: Tweet count from active graph.
        """
		print("Counting total tweets...")
		search_str = "MATCH (t:Tweet) RETURN COUNT(t)"
		return self.database.run(search_str).evaluate()

	def get_total_tweets_per_language(self, *language):
		"""
        Query to get tweet count per language from active graph.

        Args:
            language (str): Language abbreviation.

        Returns:
            dataframe: With tweet count per language from active graph.
        """
		insertion = ""
		if language:
			print("Counting tweets for language '{}'...".format(language))
			insertion = "WHERE l.lang = '{}'".format(language)
		else:
			print("Counting tweets per langauge...")

		search_str = """
		MATCH(t:Tweet)-[:WRITTEN_IN]->(l:Language) {}
		RETURN l.lang AS language, COUNT(t) AS count
		""".format(insertion)
		return self.database.run(search_str).to_data_frame()

	def get_total_tweets_daily(self):
		"""
        Query to get tweet count per date from active graph.

        Returns:
            dataframe: With tweet count per date from active graph.
        """
		print("Counting daily tweets...")
		search_str = """
		MATCH(t:Tweet)-[:WRITTEN_ON]->(d:Date) 
		RETURN d.date AS date, COUNT(t) AS total_tweets
		"""
		return self.database.run(search_str).to_data_frame()

	def get_total_tweets_per_language_daily(self, *language):
		"""
        Query to get tweet count per language per date from active graph.

        Args:
            language (str): Language abbreviation.

        Returns:
            dataframe: With tweet count per language per date from active graph.
        """
		insertion = ""
		if language:
			print("Counting daily tweets for language '{}'...".format(language))
			insertion = "WHERE l.lang = '{}'".format(language)
		else:
			print("Counting daily tweets per language...")

		search_str = """
		MATCH (d:Date)-[:WRITTEN_ON]-(t:Tweet)-[:WRITTEN_IN]->(l:Language) {}
		RETURN d.date AS date, l.lang AS language, COUNT(t) AS count
		""".format(insertion)
		return self.database.run(search_str).to_data_frame()
	
	def get_active_users_daily(self, key_player=False):
		"""
        Query to get active users per date from active graph.

        Returns:
            dataframe: With active users per date from active graph.
        """
		insertion = ""
		if key_player == True:
			print("Counting daily active key players...")
			insertion = "WHERE u.key_player"
		else:
			print("Counting daily active users...")

		search_str = """
		MATCH(u:User)-[:WROTE_ON]->(d:Date) {}
		RETURN d.date AS date, COUNT(u) AS active_users
		""".format(insertion)
		return self.database.run(search_str).to_data_frame()

	def get_tweets_per_user(self, key_player=False):
		"""
        Query to get tweet count per user from active graph.

        Args:
            key_player (bool): Check if key_player.

        Returns:
            dataframe: With tweet count per user from active graph.
        """
		insertion = ""
		if key_player == True:
			print("Counting tweets for key players...")
			insertion = "WHERE u.key_player"
		else:
			print("Counting tweets per user...")

		search_str = """
		MATCH (t:Tweet)<-[:WROTE]-(u:User) {}
		RETURN u.name AS username, COUNT(t) AS count ORDER BY count(t) DESC
		""".format(insertion)
		return self.database.run(search_str).to_data_frame()

	def get_tweet_texts_per_date(self, date, property_name, language = 'en'):
		"""
        Query to get tweet texts per date from active graph.

        Args:
            date (str): Date which will be inserted into query.
            property_name (str): Property name to define which text to get.
            language (str): Language abbreviation.

        Returns:
            dataframe: With tweet texts per date from active graph.
        """
		query = """
		MATCH (l:Language)<-[:WRITTEN_IN]-(t:Tweet)-[:WRITTEN_ON]->(d:Date) 
		WHERE d.date = {} AND l.lang = '{}'
		RETURN t.tweet_id, t.{}, d.date
		""".format('{date}', language, property_name)
		return self.database.run(query, date=str(date)).to_data_frame()

	def get_sentiment_ratings_per_date(self, date, get_user, language = 'en'):
		"""
        Query to get sentiment ratings per date from active graph.

        Args:
            date (str): Date which will be inserted into query.
            get_user (str): Check if get data from all users or only key_player.
            language (str): Language abbreviation.

        Returns:
            dataframe: With tweet ratings and retweets per date from active graph.
        """
		if get_user == 'u':
			query = """
			MATCH (l:Language) WHERE l.lang = '{}' 
			WITH l
			MATCH (d:Date) WHERE d.date = {}
			WITH l, d
			MATCH (t:Tweet)-[:WRITTEN_IN]->(l:Language),
			(t:Tweet)-[:WRITTEN_ON]->(d:Date)
			RETURN t.tweet_id, d.date AS date, t.pos_rating, t.neg_rating, t.rts AS retweets
			""".format(language, '{date}')
		elif get_user == 'k':
			query = """
			MATCH (l:Language) WHERE l.lang = '{}' 
			WITH l
			MATCH (d:Date) WHERE d.date = {}
			WITH l, d
			MATCH (u:User) WHERE u.key_player
			WITH l, d, u
			MATCH (t:Tweet)-[:WRITTEN_IN]->(l:Language),
			(t:Tweet)-[:WRITTEN_ON]->(d:Date),
			(u:User)-[:WROTE]->(t:Tweet)
			RETURN t.tweet_id, d.date AS date, t.pos_rating, t.neg_rating, t.rts AS retweets
			""".format(language, '{date}')
		return self.database.run(query, date=str(date)).to_data_frame()