import numpy as np
import pandas as pd
from pathlib import Path
from .metric_generator import MetricGenerator

"""
Class containing all relevant functions for extracting various metrics from neo4j database and saving them
"""
class AnalysisHandler(object):

	def __init__(self, database):
		self.database = database
		self.dates = self.get_all("Date", "date")
		self.langs = self.get_all("Language", "lang")
		self.mg = MetricGenerator(self.database)

		# extracts all English texts for further analysis
		self.all_texts = self.extract_texts("en")

		# global metrics
		self.total_tweets = 0
		self.total_tweets_per_language = None

		# daily metrics
		self.total_tweets_daily = None
		self.total_tweets_per_language_daily = None
		self.sentiment_daily = None
		self.topics_daily = None

		# user metrics
		self.active_users_daily = None
		self.active_key_players_daily = None
		self.tweets_per_user = None

	def get_all(self, label, prprty):
		"""
        Gets all requested data from active graph.

        Args:
            label (str): Label name.
            prprty (str): Property name.

        Returns:
            list: List with requested data from active graph.
        """
		search_str = "MATCH (x:{0}) RETURN x.{1} as {1}".format(label, prprty)
		dict_list = self.database.run(search_str).data()
		return [dct[prprty] for dct in dict_list]

	def extract_texts(self, language):
		"""
        Gets all cleaned texts from active graph.

        Args:
            language (str): Language abbreviation.

        Returns:
            text_dict (dict): With cleaned_text from active graph.
        """
		text_dict = {}
		for date in self.dates:
			search_str = """
			MATCH (l:Language)<-[:WRITTEN_IN]-(t:Tweet)-[:WRITTEN_ON]->(d:Date) 
			WHERE d.date = '{}' AND l.lang = '{}'
			RETURN t.cleaned_text as cleaned_text""".format(date, language)
			dict_list = self.database.run(search_str).data()
			text_dict[date] = [dct['cleaned_text'] for dct in dict_list]
		return text_dict

	def calc_base_metrics(self):
		"""
        Calls all metric functions.
        """
		self.get_total_tweets()
		self.get_total_tweets_per_language()
		self.get_total_tweets_daily()
		self.get_total_tweets_per_language_daily()
		self.get_active_users_daily()
		self.get_active_key_players_daily()

	def get_total_tweets(self):
		"""
        Calls function from MetricGenerator to get tweet count from active graph.

        Returns:
            total_tweets (int): Tweet count from active graph.
        """
		self.total_tweets = self.mg.get_total_tweets()

	def get_total_tweets_per_language(self, *language):
		"""
        Calls function from MetricGenerator to get tweet count per language from active graph.

        Args:
            language (str): Language abbreviation.

        Returns:
            total_tweets_per_language (dataframe): With tweet count per language from active graph.
        """
		if language:
			self.total_tweets_per_language = self.mg.get_total_tweets_per_language(language)
		self.total_tweets_per_language = self.mg.get_total_tweets_per_language()

	def get_total_tweets_daily(self):
		"""
        Calls function from MetricGenerator to get tweet count per date from active graph.

        Returns:
            total_tweets_daily (dataframe): With tweet count per date from active graph.
        """
		self.total_tweets_daily = self.mg.get_total_tweets_daily()

	def get_total_tweets_per_language_daily(self, *language):
		"""
        Calls function from MetricGenerator to get tweet count per language per date from active graph.

        Args:
            language (str): Language abbreviation.

        Returns:
           total_tweets_per_language_daily (dataframe): With tweet count per language per date from active graph.
        """
		if language:
			self.total_tweets_per_language_daily = self.mg.get_total_tweets_per_language_daily(language)
		self.total_tweets_per_language_daily = self.mg.get_total_tweets_per_language_daily()

	def get_active_users_daily(self):
		"""
        Calls function from MetricGenerator to get active users per date from active graph.

        Returns:
            active_users_daily (dataframe): With active users per date from active graph.
        """
		self.active_users_daily = self.mg.get_active_users_daily()

	def get_active_key_players_daily(self):
		"""
        Calls function from MetricGenerator to get active key_players per date from active graph.

        Returns:
            active_key_players_daily (dataframe): With active key_players per date from active graph.
        """
		self.active_key_players_daily = self.mg.get_active_users_daily(key_player=True)

	def get_tweets_per_user(self, key_player=False):
		"""
        Calls function from MetricGenerator to get tweet count per user from active graph.

        Args:
            key_player (bool): Check if key_player.

        Returns:
            tweets_per_user (dataframe): With tweet count per user from active graph.
        """
		if key_player:
			self.tweets_per_user = self.mg.get_tweets_per_user(key_player)
		self.tweets_per_user = self.mg.get_tweets_per_user()

	def save_base_metrics(self):
		"""
        Aggregates the base metrics and saves them as CSV file in given file path.
        """
		base_path = Path().absolute() / "data" / "analysis_results" 
		base_metrics = self.aggregate_base_metrics()
		output_path = base_path / "base_metrics.csv"
		base_metrics.to_csv(str(output_path), sep='\t')

		output_path = base_path / "global_metrics.txt"
		with open(str(output_path),"w") as text_file:
			text_file.write("Total number of tweets: {}\n".format(self.total_tweets))
			for key in self.total_tweets_per_language:
				text_file.write("Tweets in {}: {}: ".format(key, self.total_tweets_per_language[key]))

		print("Saved results of basic analysis to {}".format(base_path))

	def aggregate_base_metrics(self):
		"""
        Aggregates the base metrics.

        Returns:
            base_df (dataframe): With the base metrics.
        """
		feature_list = ['total_tweets']+self.langs+['active_users', 'active_key_players']
		base_df = pd.DataFrame(0, index=self.dates, columns=feature_list)
		for date in self.dates:
			base_df.at[date, 'total_tweets'] = self.total_tweets_daily[(self.total_tweets_daily['date'] == date)]['total_tweets']
			base_df.at[date, 'active_users'] = self.active_users_daily[(self.active_users_daily['date'] == date)]['active_users']
			try:
				base_df.at[date, 'active_key_players'] = self.active_key_players_daily[(self.active_key_players_daily['date'] == date)]['active_users']
			except:
				pass
			langs_that_day = self.total_tweets_per_language_daily.loc[self.total_tweets_per_language_daily['date'] == date, :]
			for lang in self.langs:
				try:
					count = list(langs_that_day.loc[langs_that_day['language'] == lang]['count'])[0]
					base_df.at[date, lang] = count
				except:
					pass
		return base_df