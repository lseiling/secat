import pandas as pd
from pathlib import Path
from data_preprocessing import *
from sklearn.decomposition import NMF
from sklearn.decomposition import LatentDirichletAllocation
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity

"""
Class containing all relevant functions for sentiment analysis of given text data
"""
class TopicAnalyser(object):

	def __init__(self, database):
		self.database = database

		#generel settings
		self.number_of_topics = 10
		self.no_top_words = 20

		#vector settings
		self.max_df = 1
		self.min_df = 1
		self.token_pattern = '\w+|\$[\d\.]+|\S+'

		#LDA settings
		self.lda_random_state = 0

		#NMF settings
		self.nmf_random_state = 0
		self.alpha = .1
		self.l1_ratio = .5

	def calculate_topics(self, tweet_df, model_option):
		"""
        Takes a given dataframe, prepares the data and then uses LDA and/or NMF model given the model_option.

        Args:
            tweet_df (dataframe): Tweet texts and dates from active graph.
            model_option (list): Containing strings which indicate the models to use.

        Returns:
            dataframe: Formated topic dataframe.
        """
		vectorizer = CountVectorizer(max_df=self.max_df, min_df=self.min_df, token_pattern=self.token_pattern)
		tf = vectorizer.fit_transform(tweet_df['t.tokenized_text']).toarray()
		tf_feature_names = vectorizer.get_feature_names()

		if model_option == 'LDA': model = LatentDirichletAllocation(n_components=self.number_of_topics, random_state=self.lda_random_state)
		elif model_option == 'NMF': model = NMF(n_components=self.number_of_topics, random_state=self.nmf_random_state, alpha=self.alpha, l1_ratio=self.l1_ratio)

		model.fit(tf)

		return self.format_topics(model, tf_feature_names, self.no_top_words)

	def format_topics(self, model, feature_names, no_top_words):
		"""
        Creates dataframe from given parameters.

        Args:
            model (): Fitted model.
            feature_names (): Feature names.
            no_top_words (int): Number of top words.

        Returns:
            dataframe: Formated topic dataframe.
        """
		topic_dict = {}
		for topic_idx, topic in enumerate(model.components_):
			topic_dict["Topic %d words" % (topic_idx)]= ['{}'.format(feature_names[i])
				for i in topic.argsort()[:-no_top_words - 1:-1]]
			topic_dict["Topic %d weights" % (topic_idx)]= ['{:.1f}'.format(topic[i])
				for i in topic.argsort()[:-no_top_words - 1:-1]]
		return pd.DataFrame(topic_dict)

	def save_topic_model(self, df, model, mode):
		"""
        Saves given dataframe as CSV file.

        Args:
            df (dataframe): Containing the topic model data.
            model (str): Name of the used model.
            mode (str): Differentiation between topics and cos_sim mode.
        """
		base_path = Path().absolute() / "data" / "analysis_results" 
		topic_model_results = df
		output_path = base_path / "{}_{}_topic_model.csv".format(model, mode)
		topic_model_results.to_csv(str(output_path), sep='\t')
		if mode == "topics":
			print("Saved topic words and weights to '{}'".format(output_path))
		else:
			print("Saved cosine similarity for each topic to '{}'".format(output_path))

	def calculate_cosine_similarity(self, all_topics):
		"""
        Calculates the cosine similarity of given topic model data.

        Args:
            all_topics (dataframe): Topic model data.

        Returns:
            res_df (dataframe): Cosine similarity data.
        """
		topic_dict = {}
		dates = [date for date in all_topics.Date.unique()]

		# extract topics from data frame
		for date in dates:
			topics_day = all_topics[all_topics.Date == date]

			for i in range (0,10):
				col_name = "Topic {} words".format(i)
				key = str(date) + "_topic" + str(i)
				topic_dict[key] = " ".join(list(topics_day[col_name]))

		# create word count matrix for all topics
		count_vec = CountVectorizer()
		sparse_matrix = count_vec.fit_transform(list(topic_dict.values()))
		doc_term_matrix = sparse_matrix.todense()

		# calculate cosine similarity
		cos_sim = cosine_similarity(doc_term_matrix)
		df = pd.DataFrame(cos_sim, 
		                  columns=topic_dict.keys(), 
		                  index=topic_dict.keys())

		# get maximum value for all following days
		cos_sims_topics = {}
		for index, row in df.iterrows():
		    row_vals = list(row)
		    chunks = [row_vals[x:x+self.number_of_topics] for x in range(0, len(row_vals), self.number_of_topics)]
		    max_vals = []
		    for chunk in chunks:
		        max_vals.append(max(chunk))
		    cos_sims_topics[index] = max_vals

		# make dictionary into dataframe
		res_df = pd.DataFrame.from_dict(cos_sims_topics)
		res_df.index = dates

		return res_df