import shlex
import subprocess
import pandas as pd
from pathlib import Path
from data_preprocessing.data_handler import *
from analysis.metric_generator import MetricGenerator

"""
Class containing all relevant functions for sentiment analysis of given text data
"""
class SentimentAnalyser(object):

	def __init__(self, database):
		self.database = database
		self.mg = MetricGenerator(self.database)

	def save_as_csv(self, df, file_location):
		"""
        Saves a dataframe as CSV file.

        Args:
            df (dataframe): Any dataframe.
            file_location (str): Location of the file ouput.
        """
		df.to_csv(file_location, sep=',',index=False)
		print('CSV file saved at location: ', file_location)

	def get_sentiment_rating(self, df):
		"""
        Takes a given dataframe, converts the data into list and passes the data to SentiStrength to get the Sentiment Analysis ratings.
        These ratings are then enriched into the given dataframe.

        Args:
            df (dataframe): Containing cleaned_text, tweet_id and date from active graph.

        Returns:
            df (dataframe): The input dataframe with the enriched SentiStrength sentiment ratings.
        """
		tweet_corpus_list = list(df['t.cleaned_text'])
		tweet_id_list = list(df['t.tweet_id'])
		tweet_date_list = list(df['d.date'])
		tweet_corpus_str = '\n'.join(tweet_corpus_list)

		java_path = Path().absolute() / "src" / "SentiStrength" / "SentiStrengthCom.jar"
		data_path = Path().absolute() / "src" / "SentiStrength" / "SentiStrength_Data/"

		p = subprocess.Popen(shlex.split("java -jar {} stdin sentidata {}".format(java_path, data_path)),stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
		b = bytes(tweet_corpus_str, 'utf-8')

		stdout_byte, stderr_text = p.communicate(b)

		sentiment_rating_str = stdout_byte.decode("utf-8") 

		sentiment_rating_str = sentiment_rating_str.rstrip().replace("\t"," ")

		sentiment_rating_list = [e[:4] for e in list(sentiment_rating_str.split('\n'))]

		pos_rating_list = []
		neg_rating_list = []

		for rating in list(sentiment_rating_str.split('\n')):
			pos_rating_list.append(rating[:1])
			neg_rating_list.append(rating[2:4])

		dic = {"tweet_id": tweet_id_list, "tweet_date": tweet_date_list, "text": tweet_corpus_list, "pos_rating": pos_rating_list, "neg_rating": neg_rating_list}
		df = pd.DataFrame.from_dict(dic, orient='index')

		return df.transpose()

	def calculate_sentiment_tweet_df(self, date_range, read_property):
		"""
        Gets all tweets with given property for given date range from graph and passes it to self.sa.get_sentiment_rating for SentiStrength.

        Args:
            date_range (list): List of dates.
			read_property (str): Property to read from graph.

        Returns:
            dataframe: Dataframe with the enriched SentiStrength sentiment ratings.
        """
		tweet_df = pd.DataFrame()

		for date in date_range:
			tweets_per_date_df = self.mg.get_tweet_texts_per_date(date, read_property)
			tweet_df = tweet_df.append(tweets_per_date_df)

			print('Generated sentiment rating for date: ', date)

		return self.get_sentiment_rating(tweet_df)

	def get_sentiment_tweet_df(self, date_range, get_user):
		"""
        Gets all tweets and their ratings for given date range from graph.

        Args:
            date_range (list): List of dates.
			get_user (str): Flag to read all user data or only key_player.

        Returns:
            tweet_df (dataframe): Dataframe with tweets and their sentiment ratings.
        """
		tweet_df = pd.DataFrame()

		for date in date_range:
			print('Get Sentiment ratings from date: ', date)
			tweets_per_date_df = self.mg.get_sentiment_ratings_per_date(date, get_user)
			tweet_df = tweet_df.append(tweets_per_date_df)

		print('Extracted requested sentiment data from graph.')
		return tweet_df