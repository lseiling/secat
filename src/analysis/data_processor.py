import pandas as pd
from data_preprocessing import *
#from data_preprocessing import TweetCleaner
from pathlib import Path
"""
Class containing framework to analyse data stored in sql database
"""

class DataProcessor(object):

	def __init__(self, database):
		self.database = database
		self.dates = self.get_all("Date", "date")
		self.langs = self.get_all("Language", "language")
		#self.tweet_cleaner = TweetCleaner()
		#self.base_data = self.calculate_basic_metrics(truncated=True)
		#output_path = Path().absolute() / "src" / "analysis" / "base_data.csv"
		#self.base_data.to_csv(str(output_path), sep='\t')
		#self.texts_for_dates = self.get_text_for_dates("en")
		#self.cleaned_texts = self.clean_text_for_dates()

	def get_all(self, label, prprty):
		search_str = 'MATCH (x:{0}) RETURN x.{1} as {1}'.format(label, prprty)
		dict_list = self.database.run(search_str).data()
		return [dct[prprty] for dct in dict_list]

	def calculate_basic_metrics(self, truncated):
		print("CALCULATING BASIC METRICS...")
		summary_by_date = self.get_tweets_per_day()
		all_langs = [self.get_lang_freqs(date, truncated) for date in self.dates]
		combined_df = self.merge_dfs(all_langs)
		res_df = pd.merge(summary_by_date, combined_df, how='outer')
		res_df.fillna(0)
		print(res_df.to_string())
		return res_df

	def merge_dfs(self, df_list):
		df = df_list[0]
		for i in range(1, len(df_list)):
			df = pd.merge(df, df_list[i], how='outer')
		return df

	def get_lang_freqs(self, date, truncated):
		search_str = """
		MATCH (d:Date)<-[:WRITTEN_ON]-()-[:WRITTEN_IN]-(l:Language) 
		WHERE d.date = '{}'
		RETURN DISTINCT d.date as date, l.lang as lang, size((l)<-[:WRITTEN_IN]-()-[:WRITTEN_ON]->(d)) as n
		""".format(date)
		if truncated:
			search_str += ' ORDER BY n DESC LIMIT 5'
		table = self.database.run(search_str).to_data_frame()
		print(table)
		table_t = table.pivot(index='date', columns="lang", values="n")
		table_t.fillna(0, inplace=True)
		table_t.reset_index(inplace=True)
		return table_t

	def get_tweets_per_day(self):
		search_str = 'MATCH (d:Date) RETURN d.date as date, size((d)<-[:WRITTEN_ON]-()) as total_tweets'
		return self.database.run(search_str).to_data_frame()

	def get_text_for_dates(self, language):
		res_dict = {}
		for date in self.dates:
			search_str = """MATCH (l:Language)<-[:WRITTEN_IN]-(t:Tweet)-[:WRITTEN_ON]->(d:Date)
			WHERE d.date = "{}" AND l.language = "{}"
			RETURN t.text as text""".format(date, language)
			dict_list = self.database.run(search_str).data()
			res_dict[date] = [dct['text'] for dct in dict_list]
		return res_dict

	def clean_text_for_dates(self):
		print("CLEANING TEXT DATA...")
		tc = TweetCleaner()
		res_dict = {}
		for date in self.dates:
			res_dict[date] = tc.clean_and_filter(self.texts_for_dates[date])
			print("Before cleaning: {} texts, after: {} texts".format(len(self.texts_for_dates[date]), len(res_dict[date])))
		return res_dict

	def get_tweets_data(self, language):
		search_str = """
		MATCH (l:Language)<-[:WRITTEN_IN]-(t:Tweet)-[:WRITTEN_ON]->(d:Date)
		WHERE l.language = {}
		RETURN t.id as id, t.text as text, l.language as lang, d.date as date, t.retweets as rts, t.favorites as favs, t.link as link
		""".format(language)
		return self.database.run(search_str).to_data_frame()

	def get_key_player(self):
		return self.database.run("MATCH (u:User {key_player: True}) RETURN u.name").to_data_frame()

	def clean_tweet_text(self):
		print("CLEANING Tweet Texts...")
		search_str = """
		MATCH (t:Tweet)
		RETURN t.tweet_id, t.text
		"""

		tweet_text_df = self.database.run(search_str).to_data_frame()
		tweet_text_df['t.cleaned_text'] = tweet_text_df['t.text'].apply(TweetCleaner.clean_pipeline)
		#tweet_text_df['t.cleaned_text'] = tweet_text_df['t.text'].apply(self.tweet_cleaner.clean_pipeline)

		return tweet_text_df

	def get_dates(self):
		return self.database.run("MATCH (d:Date) RETURN d.date AS date").to_data_frame()

	def tweet_count(self):
		#print("Counting Tweets...")
		return self.database.run("match (t:Tweet) return count(t)").evaluate()

	def date_tweet_count(self):
		#print("Counting Tweets...")
		return self.database.run("MATCH(t:Tweet)-[:WRITTEN_ON]->(d:Date) RETURN d.date AS date, COUNT(t) AS count").to_data_frame()

	def language_tweet_count(self):
		#print("Counting Tweets...")
		return self.database.run("MATCH(t:Tweet)-[:WRITTEN_IN]->(l:Language) RETURN l.lang AS language, COUNT(t) AS count").to_data_frame()

	def date_language_tweet_count(self):
		#print("Counting Tweets...")
		return self.database.run("MATCH (d:Date)-[:WRITTEN_ON]-(t:Tweet)-[:WRITTEN_IN]->(l:Language) RETURN d.date AS date, l.lang AS language, COUNT(t) AS count").to_data_frame()

	def date_en_tweet_count(self):
		#print("Counting Tweets...")
		return self.database.run("MATCH (d:Date)-[:WRITTEN_ON]-(t:Tweet)-[:WRITTEN_IN]->(l:Language) WHERE l.lang = 'en' RETURN d.date AS date, l.lang AS language, COUNT(t) AS count").to_data_frame()
	
	def date_user_count(self):
		#print("Counting Tweets...")
		return self.database.run("MATCH(u:User)-[:WROTE_ON]->(d:Date) RETURN d.date AS date, COUNT(u) AS count").to_data_frame()

	def date_key_player_count(self):
		#print("Counting Tweets...")
		return self.database.run("MATCH(u:User)-[:WROTE_ON]->(d:Date) WHERE u.key_player RETURN d.date AS date, COUNT(u) AS count").to_data_frame()

	def user_tweet_count(self):
		#print("Counting Tweets...")
		return self.database.run("MATCH (t:Tweet)<-[:WROTE]-(u:User) RETURN u.name AS username, COUNT(t) AS count ORDER BY count(t) DESC").to_data_frame()

	def key_player_tweet_count(self):
		#print("Counting Tweets...")
		return self.database.run("MATCH (t:Tweet)<-[:WROTE]-(u:User) WHERE u.key_player RETURN u.name AS username, COUNT(t) AS count ORDER BY count(t) DESC").to_data_frame()

	def tweet_texts_per_date(self, date):
		return self.database.run("MATCH(t:Tweet)-[:WRITTEN_ON]->(d:Date) WHERE d.date = {date} RETURN t.cleaned_text AS tweet", date=str(date)).to_data_frame()