import time
import langid
from GetOldTweets3.manager import *
from .tweet_cleaner import TweetCleaner
from datetime import timedelta, date, datetime

"""
Class containing all relevant scraping functions
"""
class TweetScraper(object):

    def __init__(self, query_string, start_date, end_date, num_tweets, runtime_start):
        self.query_string = query_string
        self.start_date = start_date
        self.end_date = end_date
        self.num_tweets = num_tweets
        self.runtime_start = runtime_start
        self.tweet_info = self.get_twitter_data()

    def extract_info(self, tweet):
        """
        Fomates the returned Twitter data of GetOldTweets3 to dictionary.

        Args:
            tweet (list): List of one tweets data and meta-data.

        Returns:
            info (dict): Containing data to write into active graph.
        """
        info = {}
        info['id'] = str(tweet.id)
        info['link'] = tweet.permalink
        info['username'] = self.remove_swirl(tweet.username)
        info['text'] = tweet.text
        info['date'] = tweet.date.strftime('%Y-%m-%d')
        info['time'] = tweet.date.strftime('%H:%M:%S')
        info['retweets'] = tweet.retweets
        info['favorites'] = tweet.favorites
        info['geo'] = tweet.geo
        
        tc = TweetCleaner()
        info['cleaned_text'] = tc.clean_pipeline(tweet.text)
        info['tokenized_text'] = tc.tokenize_pipeline(tweet.text)
        info['language'] = langid.classify(info['cleaned_text'])[0]
        
        if tweet.to is not None and len(tweet.to)!=0:
            info['addressed'] = [self.remove_swirl(addressed) for addressed in tweet.to.split(" ")]
        else:
            info['addressed'] = None
        if tweet.mentions is not None and len(tweet.mentions)!=0:
            info['mentions'] = [self.remove_swirl(mention) for mention in tweet.mentions.split(" ")]
        else:
            info['mentions'] = None
        if tweet.hashtags is not None and len(tweet.hashtags)!=0:
            info['hashtags'] = [hashtag for hashtag in tweet.hashtags.split(" ")]
        else:
            info['hashtags'] = None
        return info
        
    def get_twitter_data(self, tries = 100):
        """
        Scrapes Twitter data using GetOldTweets3.

        Args:
            tries (int): Number of retries per date if Twitter throws an error.

        Returns:
            tweet_info (list): Containing tweet data to write into active graph.
        """
        all_tweets = []
        tweetCriteria = TweetCriteria().setQuerySearch(self.query_string).setSince(self.start_date).setUntil(self.end_date).setMaxTweets(self.num_tweets)
        print("{} [{} sec] Scraping tweets for time period {} - {}".format(datetime.now().strftime("%Y-%m-%d %H:%M"), round(time.time() - self.runtime_start, 2), self.start_date,self.end_date))
        for i in range(tries):
            try:
                all_tweets = TweetManager.getTweets(tweetCriteria)
            except Exception as e:
                if i < tries - 1:
                    print("{} [{} sec] Try {}: Failed to retrieve and write tweets for {} - {}. Retrying...".format(datetime.now().strftime("%Y-%m-%d %H:%M"), round(time.time() - self.runtime_start, 2), i+1, self.start_date, self.end_date))
                    continue
                else:
                    raise
                    return None
            break
        print("{} [{} sec] Retrieved {} tweets for {} - {}".format(datetime.now().strftime("%Y-%m-%d %H:%M"), round(time.time() - self.runtime_start, 2), len(all_tweets), self.start_date, self.end_date))
        tweet_info = [self.extract_info(tweet) for tweet in all_tweets]
        return tweet_info

    def get_info(self):
        """
        Starts the scraper using GetOldTweets3.

        Returns:
            tweet_info (list): Containing tweet data to write into active graph.
        """
        return self.tweet_info

    def remove_swirl(self, input_string):
        """
        Removes all @ symbols from given string.

        Returns:
            input_string (str): String without @ symbols.
        """
        return input_string.replace("@","").strip()