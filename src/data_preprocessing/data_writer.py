import numpy as np
from tqdm import tqdm
from py2neo import Graph
 
"""
Class containing all relevant functions for writing data into Neo4j graph
"""
class DataWriter(object):
    def __init__(self, database):
        self.graph = database

# WRITE SCRAPED DATA
    def write_dicts(self, dict_list, batch_size = 20000):
        """
        Query to write scraped tweet data in batches into active graph.

        Args:
            dict_list (dict): Scraped data.
            batch_size (int): Number of entities to write into active graph.
        """
        query = """
        UNWIND {input_data} AS i
        MERGE (u:User {name: i.username})
        CREATE (t:Tweet {text: i.text, cleaned_text: i.cleaned_text, tokenized_text: i.tokenized_text, tweet_id: i.id, link: i.link, rts: i.retweets, favs: i.favorites, geo: i.geo, time: i.time})
        MERGE (d:Date {date: i.date})
        MERGE (l:Language {lang: i.language})
        CREATE (u)-[:WROTE]->(t)
        MERGE (u)-[:WROTE_ON]->(d)
        CREATE (t)-[:WRITTEN_ON]->(d)
        MERGE (u)-[:WROTE_IN]->(l)
        CREATE (t)-[:WRITTEN_IN]->(l)
        WITH i, u, t, d
        UNWIND i.hashtags AS ht
        FOREACH (hshtg IN ht| 
         MERGE (h:Hashtag {tag: hshtg})
         MERGE (u)-[:USED]->(h)
         CREATE (t)-[:TAGGED]->(h)
         MERGE (h)-[:USED_ON]->(d))
        WITH i, u, t
        UNWIND i.addressed AS a
        FOREACH (ad_user IN a| 
         MERGE (u1:User {name: ad_user})
         MERGE (t)-[:ADDRESSED]->(u1)
         MERGE (u)-[:ADDRESSED]->(u1))
        WITH i, u, t
        UNWIND i.mentions AS m
        FOREACH (ad_user IN m| 
         MERGE (u2:User {name: ad_user})
         MERGE (t)-[:MENTIONED]->(u2)
         MERGE (u)-[:MENTIONED]->(u2))"""
        batches = [dict_list[i:i + batch_size] for i in range(0, len(dict_list), batch_size)]
        for batch in tqdm(batches):
            self.graph.run(query, input_data=batch)

# WRITE ENRICHED TWEET DATA
    def write_key_player(self, user_list):
        """
        Query to write key player data into active graph.

        Args:
            user_list (list): Containing strings of user names.
        """
        print("\nWriting key players to database...")
        query = """
        UNWIND {input_data} as i 
        MATCH (u:User {name: i})
        SET u.key_player = TRUE
        """
        self.graph.run(query, input_data=user_list)
        print("Finished writing key players to database...\n")

    def write_enriched_data(self, user_data, write_tweets=False):
        """
        Query to write enriched data into active graph.

        Args:
            user_data (list): Containing strings of user names.
            write_tweets (bool): Checks if to enrich only user data or user data and their tweets.
        """
        query = """
        UNWIND {input_data} as i  
        MATCH (u:User {name: i.screen_name}) 
        SET u.id = i.id, u.verified = i.verified, u.followers_count = i.followers_count, u.friends_count = i.friends_count"""
        
        if write_tweets:
            query += """
            WITH i
            UNWIND i.tweets AS twt
            MATCH (t:Tweet {tweet_id: twt.id}) 
            SET t.source = twt.source
            WITH t, twt
            UNWIND twt.in_reply_to_screen_name as reply
            MATCH (u:User {name: reply}) 
            MERGE (t)-[r:IN_REPLY_TO]->(u)
            """
        self.graph.run(query, input_data = user_data)

#WRITE CLEAN TWEET DATA
    def write_cleaned_tweet_text(self, tweet_text_df, property_name, batch_size = 20000):
        """
        Query to write cleaned tweet data in batches into active graph.

        Args:
            tweet_text_df (dict): Scraped data.
            property_name (str): Property name to define which text to get.
            batch_size (int): Number of entities to write into active graph.
        """
        if property_name == '': property_name = 'cleaned_text'
        print("Writing cleaned text into database...")
        query = """
        UNWIND {} AS i
        MERGE (t:Tweet {})
        SET t.{} = i.{}
        SET t.shorter4 = i.shorter4
        SET t.empty = i.empty
        """.format('{input_data}', '{tweet_id: i.tweet_id}', property_name, property_name)
        if tweet_text_df.shape[0] < batch_size: batch_size = tweet_text_df.shape[0]
        batches = np.array_split(tweet_text_df, int(tweet_text_df.shape[0]/batch_size))
        for batch in tqdm(batches):
            batch_dict = batch.to_dict('records')
            self.graph.run(query, input_data=batch_dict)
        print("Finished writing cleaned texts into database.")            

    def delete_duplicate_relations(self):
        """
        Query to delete the IS_DUPLICATE_OF relation between nodes in active graph.
        """
        create_duplicate_relation = """
        MATCH (t1:Tweet)-[r:IS_DUPLICATE_OF]-(t2:Tweet)
        DELETE r
        """
        self.graph.run(create_duplicate_relation)

    def write_duplicate_relations(self):
        """
        Query to write the IS_DUPLICATE_OF relation between nodes into active graph.
        """
        print("Deleting previous duplicate relationships between nodes...")
        self.delete_duplicate_relations()

        print("Searching for duplicate cleaned texts and creating relationship between duplicate tweet nodes...")
        create_duplicate_relation = """
        MATCH (t1:Tweet)
        WITH t1.cleaned_text as cleaned_texts, collect(t1) AS duplicate_tweets
        WHERE size(duplicate_tweets) > 1
        WITH duplicate_tweets[0] as firstNode, duplicate_tweets[1..] as otherNodes
        UNWIND otherNodes as other
        CREATE (firstNode)-[r:IS_DUPLICATE_OF]->(other)
        SET firstNode.duplicate = TRUE
        SET other.duplicate = TRUE
        """
        self.graph.run(create_duplicate_relation)

        input("Created relationship between duplicates. Press Enter to continue")

#WRITE ANALYSIS DATA
    def write_sentiment_ratings(self, df, batch_size = 20000):
        """
        Query to write the sentiment ratings in batches into active graph.

        Args:
            df (dataframe): Tweet data.
            batch_size (int): Number of entities to write into active graph.
        """
        print("Writing Sentiment ratings into database...")
        query = """
        UNWIND {} AS i
        MERGE (t:Tweet {})
        SET t.pos_rating = i.pos_rating
        SET t.neg_rating = i.neg_rating
        """.format('{input_data}', '{tweet_id: i.tweet_id}')
        if df.shape[0] < batch_size: batch_size = df.shape[0]
        batches = np.array_split(df, int(df.shape[0]/batch_size))
        for batch in tqdm(batches):
            batch_dict = batch.to_dict('records')
            self.graph.run(query, input_data=batch_dict)
        print("Finished writing Sentiment ratings into database.")
