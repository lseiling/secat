import os
import re
import platform
from pathlib import Path

"""
Class containing all relevant functions for saving and loading neo4j graph dumps
"""
class DumpHandler(object):

	def __init__(self):
		self.repo_path = Path().absolute()
		if platform.system() == "Windows":
			self.path_to_databases = Path.home() / ".Neo4jDesktop" / "neo4jDatabases"
		else:	
			self.path_to_databases = Path.home() / ".config" / "Neo4j Desktop" / "Application"/ "neo4jDatabases"
		self.path_to_database = self.get_path_to_db()

	def load_dump(self, filename):
		"""
        Executes the neo4j-admin command in the operation systems command line to load a database dump into the Neo4j graph.

        Args:
            filename (str): Name of the dump.
        """
		path_to_file, path_to_database = self.return_file_and_db_locations(filename)
		if platform.system() == "Windows":
			os.system("cd {}\\bin && neo4j-admin load --from={} --database=graph.db --force".format(path_to_database,path_to_file))
		else:
			os.system("cd {};bin/neo4j-admin load --from={} --database=graph.db --force".format(path_to_database,path_to_file))

	def save_dump(self, filename):
		"""
        Executes the neo4j-admin command in the operation systems command line to save a database dump into file system.

        Args:
            filename (str): Name of the dump.
        """
		path_to_file, path_to_database = self.return_file_and_db_locations(filename)
		if platform.system() == "Windows":
			os.system("cd {}\\bin;neo4j-admin dump --database=graph.db --to={}".format(path_to_database, path_to_file))
		else:
			os.system("cd {};bin/neo4j-admin dump --database=graph.db --to={}".format(path_to_database, path_to_file))

	def return_file_and_db_locations(self, filename):
		"""
        Creates the os dependent strings to file and database path.

        Args:
            filename (str): Name of the dump.

        Returns:
            path_to_file (str): Path to the file.
            path_to_database (str): Path to the database.
        """
		if platform.system() == "Windows":
			path_to_file = str(self.repo_path / "data" / "databases")+"\\"+filename
			path_to_database = str(self.path_to_database)
		else:
			path_to_file = re.escape(str(self.repo_path / "data" / "databases"))+"/"+filename
			path_to_database = re.escape(str(self.path_to_database))
		return path_to_file, path_to_database

	def get_content_list(self, path):
		"""
        Gets the os dependent strings to file and database path.

        Args:
            path (str): Path to the dump.

        Returns:
            list: Path to the file and path to the database.
        """
		return [x for x in path.iterdir() if x.is_dir()]

	def get_path_to_db(self):
		"""
        Gets the os dependent strings to database path.

        Returns:
            path_to_database (list): Path to the file and path to the database.
        """
		db_dirs = self.get_content_list(self.path_to_databases)
		if len(db_dirs) > 1:
			raise LookupError("More than 1 neo4j databases were found. Please make sure that you have exactly one neo4j database.")
		elif len(db_dirs) == 0:
			raise LookupError("No neo4j databases were found. Please make sure to initialise your neo4j database.")
		else:
			path_to_database = self.path_to_databases / db_dirs[0]
			installation = self.get_content_list(path_to_database)[0]
			path_to_database = path_to_database / installation
			return path_to_database