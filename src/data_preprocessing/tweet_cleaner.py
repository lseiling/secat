﻿import re
import nltk
import string
from tqdm import tqdm
from functools import reduce
from nltk.corpus import wordnet as wn
from nltk.stem.wordnet import WordNetLemmatizer

"""
Class containing all relevant cleaning functions
"""
class TweetCleaner(object):

    def __init__(self):
        #nltk.download('wordnet')
        self.cleaning_functions = [self.remove_urls, 
                                    self.remove_usernames, 
                                    self.remove_special_chars, 
                                    self.remove_excess_white_space]
        self.tokenize_functions = [self.remove_urls, 
                                    self.remove_usernames, 
                                    self.remove_emojis, 
                                    self.remove_numbers, 
                                    self.remove_special_chars_for_token, 
                                    self.remove_apostrophes_keep_contractions, 
                                    self.lower_case, 
                                    self.remove_small_words, 
                                    self.remove_sepcific_words, 
                                    self.remove_en_stopwords, 
                                    self.get_word_meanings, 
                                    self.get_root_words, 
                                    self.remove_excess_white_space]
        self.database = None
        self.tweet_table = None

    def remove_usernames(self, text):
        """
        Removes all mentioned usernames from a given input string.

        Args:
            text (str): String to be checked for Twitter username mentions.

        Returns:
            str: String with all Twitter username mentions removed.
        """
        reg_ex = r'(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z]+[A-Za-z0-9-_]+)'
        return re.sub(reg_ex,r'',text)

    def remove_emojis(self, text):
        """
        Removes all emojis from a given input string.

        Args:
            text (str): String to be checked for emojis.

        Returns:
            str: String with all emojis removed.
        """
        try:
        # UCS-4
            emoji_reg = re.compile(u'([\U00002600-\U000027BF])|([\U0001f300-\U0001f64F])|([\U0001f680-\U0001f6FF])')
        except re.error:
        # UCS-2
            emoji_reg = re.compile(u'([\u2600-\u27BF])|([\uD83C][\uDF00-\uDFFF])|([\uD83D][\uDC00-\uDE4F])|([\uD83D][\uDE80-\uDEFF])')
        return re.sub(emoji_reg,r'',text)

    def remove_urls(self, text):
        """
        Removes all urls from a given input string.

        Args:
            text (str): String to be checked for urls.

        Returns:
            str: String with all urls removed.
        """
        split_by_pics = text.split("pic.twitter")
        text = " pic.twitter".join(split_by_pics)
        url_reg = re.compile(r'(http)?s?:?\/?\/?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=])*')
        return re.sub(url_reg,r'',text)

    def remove_numbers(self, text):
        """
        Removes all numbers from a given input string.

        Args:
            text (str): String to be checked for numbers.

        Returns:
            str: String with all numbers removed.
        """
        return re.sub(r'\d',r'',text)

    def remove_special_chars(self, text, excl_chars='.,!?’\'\"'):
        """
        Removes all special characters - excluding the characters in the excl_chars string - from a given input string.

        Args:
            text (str): String to be checked for special characters.
            excl_chars (str): String of non-alphanumeric characters which are not supposed to be removed.
                                The default is standard punctuation (. , ! ? ’ ' ").

        Returns:
            str: String with all special characters removed.
        """
        reg_ex = r'[^\w '+excl_chars+']'
        return re.sub(reg_ex,r'',text)

    def remove_special_chars_for_token(self, text, excl_chars='’\''):
        """
        Removes all special characters - excluding the characters in the excl_chars string - from a given input string.

        Args:
            text (str): String to be checked for special characters.
            excl_chars (str): String of non-alphanumeric characters which are not supposed to be removed.
                                The default is standard punctuation (’ ').

        Returns:
            str: String with all special characters removed.
        """
        reg_ex = r'[^\w '+excl_chars+']'
        return re.sub(reg_ex,r'',text)

    def remove_apostrophes_keep_contractions(self, text):
        """
        Removes all apostrophes except those used in word contractions from a given input string.

        Args:
            text (str): String to be checked for apostrophes.

        Returns:
            str: String with all apostrophes except those used in word contractions removed.
        """
        reg_ex = r"(?!\b(\w*['’`´]\w*)\b)['’`´]"
        return re.sub(reg_ex,r'',text)

    def remove_excess_white_space(self, text):
        """
        Removes all excess white space from a given input string.

        Args:
            text (str): String to be checked for excess white space.

        Returns:
            str: String with all excess white space removed.
        """
        return re.sub(r' [ ]*',r' ',text).strip()

    def lower_case(self, text):
        """
        Returns an input string with all lower case characters.

        Args:
            text (str): String to be turned into lower case.

        Returns:
            text (str): Input string in lower case
        """
        return text.lower()

    def remove_en_stopwords(self, text):
        """
        Removes all English stopwords from a given input string.

        Args:
            text (str): String to be checked for stopwords.

        Returns:
            str: String with all stopwords removed.
        """
        en_stop = set(nltk.corpus.stopwords.words('english'))
        word_list = re.sub("[^\w]", " ",  text).split()
        token_list = []

        for word in word_list:
            if word not in en_stop: token_list.append(word)

        return ' '.join(token_list)

    def remove_sepcific_words(self, text, to_remove=['btc', 'bitcoin']):
        """
        Removes all specific words from a given input string.

        Args:
            text (str): String to be checked for specific words.
            to_remove (list): List with strings of words which should be removed.

        Returns:
            text (str): String with all words except those that should be removed.
        """
        for word in to_remove:
            if word in text:
                text = text.replace(word, '')

        return text

    def remove_small_words(self, text):
        """
        Removes all words that contain less than a given length of chars from a given input string.

        Args:
            text (str): String to be checked for small words.

        Returns:
            trimmed_text (str): String with all small words removed.
        """
        trimmed_text = ' '.join([w for w in text.split() if len(w)>2])

        return trimmed_text

    def get_word_meanings(self, text):
        """
        Gets the word meaning for every word in given input string and returns this as new string.

        Args:
            text (str): String to be checked for word meaning.

        Returns:
            str: String with all words replaced with their word meaning.
        """
        word_list = re.sub("[^\w]", " ",  text).split()
        lemma_words = []

        for word in word_list:
            lemma = wn.morphy(word)
            if lemma is None:
                lemma_words.append(word)
            else:
                lemma_words.append(lemma)

        return ' '.join(lemma_words)

    def get_root_words(self, text):
        """
        Gets the root word for every word in given input string and returns this as new string.

        Args:
            text (str): String to be checked for root words.

        Returns:
            str: String with all words replaced with their root word.
        """
        word_list = re.sub("[^\w]", " ",  text).split()
        root_word_list = []

        for word in word_list:
            root_word_list.append(WordNetLemmatizer().lemmatize(word))

        return ' '.join(root_word_list)

    def clean_pipeline(self, data, functions=[remove_urls, remove_usernames, remove_emojis, remove_numbers, remove_special_chars, remove_excess_white_space, lower_case]):
        """
        Sequentially applies a set of specified functions to the given data.

        Args:
            data (str): Data to be modified by function.
            functions (list): List of functions to be applied to the data, in sequential order (first applied first, second applied second, etc.)
                              The default is the following sequence: replace_html_entities, remove_urls, remove_emojis_smileys, remove_hastags, normalise_quotationmarks, replace_slang, remove_numbers, remove_special_chars, remove_duplicate_punct, remove_excess_white_space, lower_case, strip

        Returns:
            str: Data as modified by specified functions.
        """
        return reduce(lambda a, x: x(a), self.cleaning_functions, data)

    def tokenize_pipeline(self, data):
        """
        Sequentially applies a set of specified functions to the given data.

        Args:
            data (str): Data to be modified by function.

        Returns:
            str: Data as modified by specified functions.
        """
        return reduce(lambda a, x: x(a), self.tokenize_functions, data)

    def get_tweet_table(self, database, verbose = True):
        """
        Queries the connected graph for all tweet texts and their ids.

        Args:
            database (object): Object containing the active graph data.
            verbose (bool): .

        Returns:
            tweet_table (list): List of query results containing tweet_id and text.
        """
        self.database = database
        if verbose:
            self.print_cleaning_info()

        print("Obtaining all tweet texts from database...")
        search_str = "MATCH (t:Tweet) RETURN t.tweet_id AS tweet_id, t.text AS text"
        self.tweet_table = self.database.run(search_str).to_data_frame()
        print("Obtained all tweet texts from database.")
        
    def clean_tweet_table(self, cleaning_order, property_name):
        """
        Uses all given cleaning functions on tweet text and saves cleaned text with given property to the table.

        Args:
            cleaning_order (list): List containing numbers which are mapped with the respective cleaning functions.
            property_name (str): String containing a property name.

        Returns:
            tweet_table (list): List of query results enriched by cleaned text.
        """
        if property_name == '': property_name = 'cleaned_text'
        print("Cleaning all tweet texts...")
        self.set_cleaning_functions(cleaning_order)
        print("Using the following order of functions:\n{}".format([fun.__name__ for fun in self.cleaning_functions]))
        texts = list(self.tweet_table['text'])
        cleaned_texts = list(map(self.clean_pipeline, tqdm(texts)))
        self.tweet_table[property_name] = cleaned_texts
        print("Cleaned all tweet texts.\n")
        
    def set_cleaning_functions(self, cleaning_order):
        """
        Uses a list containing numbers and mappes these to the respective cleaning functions.

        Args:
            cleaning_order (list): List containing numbers.

        Returns:
            cleaning_functions (list): List of cleaning functions.
        """
        if cleaning_order[0] == 0 and len(cleaning_order) == 1:
            self.cleaning_functions = self.tokenize_functions
        else:
            nums_to_funs = {1:self.remove_urls, 2:self.remove_usernames, 3:self.remove_emojis, 4:self.remove_numbers, 5:self.remove_special_chars, 6:self.remove_excess_white_space, 7:self.lower_case, 8:self.remove_apostrophes_keep_contractions}
            fun_cleaning_order = list(map(lambda x: nums_to_funs[x], cleaning_order))
            self.cleaning_functions = fun_cleaning_order

    def print_cleaning_info(self):
        """
        Queries cleaning metrics from graph and prints these.
        """
        num_tweets = self.database.run("MATCH (t:Tweet) RETURN COUNT(t)").evaluate()
        num_cleaned = self.database.run("MATCH (t:Tweet) WHERE EXISTS(t.cleaned_text) RETURN COUNT(t)").evaluate()
        num_duplicates = self.database.run("MATCH (t:Tweet) WHERE EXISTS(t.duplicate) RETURN COUNT(t)").evaluate()
        num_shorter4 = self.database.run("MATCH (t:Tweet) WHERE t.shorter4 = TRUE RETURN COUNT(t)").evaluate()
        num_empty = self.database.run("MATCH (t:Tweet) WHERE t.empty = TRUE RETURN COUNT(t)").evaluate()
        print("""The current database holds: {} Tweets.
            {} Tweets are cleaned.
            {} Tweets are marked as duplicates.
            {} Tweets are shorter than 4 words.
            {} Tweets remain with no text.
            """.format(num_tweets, num_cleaned, num_duplicates, num_shorter4, num_empty)) 

    def add_cleaning_metrics(self, property_name):
        """
        Adds cleaning metrics to the active graph.

        Args:
            property_name (str): String containing a property name.
        """
        if property_name == '': property_name = 'cleaned_text'
        print("Adding tweet metrics to dataframe...")
        self.tweet_table['shorter4'] = len(self.tweet_table[property_name].str.split(" ")) < 4
        self.tweet_table['empty'] = self.tweet_table[property_name].str.strip() == 0
        self.tweet_table.head()
        print("Added all tweet metrics to dataframe.")

    def return_tweet_table(self):
        """
        Returns:
            tweet_table (dataframe): Tweet content from active graph.
        """
        return self.tweet_table