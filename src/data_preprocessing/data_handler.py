import pandas as pd
from time import time
from pathlib import Path
from getpass import getpass
from py2neo import Graph, Node
from multiprocessing import cpu_count
from multiprocessing.dummy import Pool
from datetime import timedelta, datetime
from .data_writer import DataWriter
from .tweet_scraper import TweetScraper
from .tweet_cleaner import TweetCleaner
from .tweet_enricher import TweetEnricher
from analysis.analysis_handler import AnalysisHandler
from analysis.topic_analyser import TopicAnalyser
from analysis.sentiment_analyser import SentimentAnalyser
from analysis.metric_generator import MetricGenerator

"""
Class containing all relevant functions for setting up neo4j database and for saving and retrieving data from them
"""
class DataHandler(object):

	def __init__(self):
		pw = getpass("Please type in the password used to initialise your graph database: ")
		self.password = pw
		self.database = None
		self.connected = False
		self.runtime_start = 0
		self.num_nodes = 0
		self.num_rels = 0
		self.key_players = None
		self.connect_to_database()
		if self.connected: 
			self.writer = DataWriter(self.database)

# SETS BASIC CONSTRAINTS ON DATABASE - ENFORCED WHENEVER READING OR WRITING RELEVANT DATA NODES
	def set_db_constraints(self):
		"""
        Queries to write database constraints into active graph.
        """
		constraint1 = """ CREATE CONSTRAINT ON (u:User)
		ASSERT u.name IS UNIQUE"""
		constraint2 = """CREATE CONSTRAINT ON (t:Tweet)
		ASSERT t.tweet_id IS UNIQUE"""
		constraint3 = """CREATE CONSTRAINT ON (d:Date)
		ASSERT d.date IS UNIQUE
		"""
		constraint4 = """CREATE CONSTRAINT ON (l:Language)
		ASSERT l.language IS UNIQUE
		"""
		constraint5 = """CREATE CONSTRAINT ON (h:Hashtag)
		ASSERT h.tag IS UNIQUE
		"""
		self.database.run(constraint1)
		self.database.run(constraint2)
		self.database.run(constraint3)
		self.database.run(constraint4)
		self.database.run(constraint5)
		 
# CONNECTION TO DATABASE
	def connect_to_database(self):
		"""
        Calls functions to connect to active graph.
        """
		try:
			self.database = Graph(password=self.password)
			self.test_db_connection(self.database)
			self.update_node_rel_counts()
			self.connected = True
		except Exception as e:
			raise LookupError("Could not connect to database.")

	def test_db_connection(self, db):
		"""
        Calls functions to test the connection to active graph.

        Args:
            db (object): Containing the active graph data.
        """
		testNode = Node("Test")
		stream = db.begin()
		stream.create(testNode)
		stream.delete(testNode)
		stream.commit()

	def update_node_rel_counts(self):
		"""
        Queries to count all nodes and relations in active graph.
        """
		self.num_nodes = self.database.run("MATCH (n) RETURN COUNT(n)").evaluate()
		self.num_rels = self.database.run("MATCH (n)-[r]->() RETURN COUNT(r)").evaluate()

# THREAD, SCRAPE AND WRITE
	def get_daterange(self, start_date, end_date):  
		"""
        Creates list of dates from given input dates.

        Args:
            start_date (str): Given start date.
            end_date (str): Given end date.

        Returns:
            dates (list): All dates in between start and end date including those.
        """
		dates = []
		for n in range(int ((end_date - start_date).days)+1):
				dates.append(start_date + timedelta(n))
		return dates

	def collect_data(self, single_date, tweet_num, query_string):
		"""
        Creates list of dates from given input dates.

        Args:
            single_date (str): Search Twitter data for given date.
            tweet_num (str): Amount of Tweets per day that should be scraped.
            query_string (str): Topics to look for on Twitter.

        Returns:
            tweet_info (list): Containing scraped tweet data.
        """
		start = single_date.strftime("%Y-%m-%d")
		end = (single_date+timedelta(days=1)).strftime("%Y-%m-%d")
		scraper = TweetScraper(query_string, start, end, tweet_num, self.runtime_start)
		return scraper.get_info()

	def write_data(self, scraped_info, start_date, end_date):
		"""
        Calls the query function to write the scraped data into active graph and printing writing progress in command line.

        Args:
        	scraped_info (dict): Scraped data.
            start_date (str): Given start date.
            end_date (str): Given end date.
        """
		print("{} [{} sec] Start writing tweets for {} - {}".format(datetime.now().strftime("%Y-%m-%d %H:%M"), round(time() - self.runtime_start, 2), start_date, end_date))
		self.writer.write_dicts(scraped_info)
		print("{} [{} sec] Finished writing tweets for {} - {}".format(datetime.now().strftime("%Y-%m-%d %H:%M"), round(time() - self.runtime_start, 2), start_date, end_date))
	
	def get_and_write_data(self, query_string, start_date, end_date, tweet_num):
		"""
        Calls functions to scrape Tweets from Twitter and write the scraped data into active graph and printing writing progress in comand line.

        Args:
        	query_string (str): Topics to look for on Twitter.
            start_date (str): Given start date.
            end_date (str): Given end date.
            tweet_num (str): Amount of Tweets per day that should be scraped.
        """
		self.runtime_start = time()
		self.set_db_constraints()

		max_threads = 50
		daterange = self.get_daterange(start_date, end_date)
		sublists = [daterange[x:x+max_threads] for x in range(0, len(daterange), max_threads)]
		if self.connected:
			for sublist in sublists:
				pool = Pool(max_threads)
				dicts_per_day = pool.map(lambda d: self.collect_data(d, tweet_num, query_string), sublist)
				all_dicts = [item for sublist in dicts_per_day for item in sublist]
				pool.close() 
				pool.join()
				self.write_data(all_dicts, sublist[0], sublist[-1])
		else:
			print("{} [{} sec] Could not start scraping process due to lacking connection to Neo4j graph.".format(datetime.now().strftime("%Y-%m-%d %H:%M"), round(time() - self.runtime_start, 2)))
		
		#self.writer.write_duplicate_relations()
		print("{} [total runtime: {} sec]".format(datetime.now().strftime("%Y-%m-%d %H:%M"), round(time() - self.runtime_start, 2)))

# ENRICH AND WRITE
	def lookup_csv(self, filename):
		"""
        Gets the path of a CSV file given its filename.

        Args:
            filename (str): Name of the CSV file.

        Returns:
            csv_path (str): Path and name of the searched CSV file.
        """
		csv_path = Path().absolute() / "data" / filename
		print(csv_path)
		if csv_path.exists():
			return csv_path
		return None

	def start_twitter_search(self):
		"""
        Sets up connection data needed to use the Twitter API.
        """
		print("\nSetting up connection to Twitter API...\nGetting authentication information from '/data/twitter_access.csv'...")
		access = False
		while not access:
			file_path = self.lookup_csv("twitter_access.csv")
			if file_path:
				try:
					self.te = TweetEnricher(file_path)
					access = True
				except Exception as e:
					print()
					print(e)
					input("\nPress Enter to try again.")
					continue
			else:
				print("\nCannot find the file '/data/twitter_access.csv'.")
				print("Please make sure that the file is in the repo's 'data' folder.\n")
				input("\nPress Enter to try again.")
				continue
			break
		print("Successfully saved authentication information from '/data/twitter_access.csv'...\n")

	def set_key_players(self, filename):
		"""
        Checks how many of the key players given from CSV file are in the active graph.

        Args:
            filename (str): Name of the CSV file.
        """
		with open(filename, encoding='utf-8-sig') as f:
			self.key_players = [line.strip() for line in f]

		num_in_db = self.database.run("MATCH (u:User) WHERE u.name IN {} RETURN COUNT(u)".format(self.key_players)).evaluate()
		print("The file contains {} usernames out of which {} appear in the database.".format(len(self.key_players),num_in_db))

		self.writer.write_key_player(self.key_players)

	def enrich_data(self, config, get_tweets):
		"""
        Starts the enriching process by calling the scraping function and writing the data into active graph. 

        Args:
            config (str): Checks if all Users or only key players are to enrich.
            get_tweets (bool): Checks if only Users are to enrich or the Tweets also.
        """
		users = []	
		if config == "single user":
			users = []
			users.append(input("Input the User you want to enrich: "))
			config = config + " " + users[0]
		else:
			insertion = ""
			if config == "key players":
				insertion = "WHERE u.key_player"
			search_str = """MATCH (u:User) {} RETURN u.name""".format(insertion)
			users = self.database.run(search_str).to_ndarray().tolist()
			users = [item for sublist in users for item in sublist]

		print("\nGetting data for {}...".format(config))
		self.te.get_data_for_users(users, get_tweets=get_tweets)
		print("Got all data for {}...".format(config))
		try:
			self.writer.write_enriched_data(self.te.user_info, write_tweets=get_tweets)
		except Exception as e:
			print(e)
		print("Wrote all data for {}...\n".format(config))

	def write_duplicate_rel(self):
		"""
        Calls writer function to write IS_DUPLICATE_OF relation between nodes in active graph.
        """
		self.writer.write_duplicate_relations()

# CLEAN AND WRITE
	def get_clean_tweet_texts(self, tc, cleaning_order, property_name):
		"""
        Gets the path of a CSV file given its filename.

        Args:
            tc (class): TweetCleaner.
            cleaning_order (list): List containing numbers.
			property_name (str): String containing a property name.

        Returns:
            dataframe: Tweet content from active graph.
        """
		tc.get_tweet_table(self.database)
		tc.clean_tweet_table(cleaning_order, property_name)
		tc.add_cleaning_metrics(property_name)
		return tc.return_tweet_table()

	def write_clean_tweet_texts(self, tweet_df, property_name):
		"""
        Calls writer function to write cleaned tweet data in batches into active graph.

        Args:
        	tweet_text_df (dict): Scraped data.
			property_name (str): Property name to define which text to get.
        """
		self.writer.write_cleaned_tweet_text(tweet_df, property_name)

	def get_and_write_clean_texts(self, cleaning_order, property_name):
		"""
        Calls functions to clean and write tweets into active graph.

        Args:
            cleaning_order (list): List containing numbers.
			property_name (str): String containing a property name.
        """
		self.runtime_start = time()
		self.set_db_constraints()

		tc = TweetCleaner()
		tweet_df = self.get_clean_tweet_texts(tc, cleaning_order, property_name)
		self.write_clean_tweet_texts(tweet_df, property_name)
		tc.print_cleaning_info()

# ANALYSE AND WRITE
	def get_dates(self):
		"""
        Query to get all dates from active graph.

        Returns:
            dataframe: With all dates from active graph.
        """
		return self.database.run("MATCH (d:Date) RETURN d.date AS date").to_data_frame()

	def start_analysis(self):
		"""
        Calls AnalysisHandler class.
        """
		self.ah = AnalysisHandler(self.database)

	def get_and_save_base_metrics(self):
		"""
        Calls metric functions to calculate base metrics and save them from AnalysisHandler class.
        """
		self.ah.calc_base_metrics()
		self.ah.save_base_metrics()

	def get_topics(self, start_date, end_date, model_option):
		"""
        Calls functions to query data from active graph, pass it to the topic models given model_option and save it as CSV file.

        Args:
            start_date (str): Given start date.
            end_date (str): Given end date.
			model_option (list): Containing strings which indicate the models to use.
        """
		ta = TopicAnalyser(self.database)
		mg = MetricGenerator(self.database)
		read_property = 'tokenized_text'

		for model in model_option:
			print('Start calculation for {}'.format(model))
			topic_df = pd.DataFrame()
			for date in self.get_daterange(start_date, end_date):
				tweets_per_date_df = mg.get_tweet_texts_per_date(date, read_property, language = "en")
				print('{} on date {}'.format(model, date))
				topic_date_df = ta.calculate_topics(tweets_per_date_df, model)
				topic_date_df.insert(0, "Date", date, True)
				topic_df = topic_df.append(topic_date_df)

			ta.save_topic_model(topic_df, model, "topics")
			
			cos_sims = ta.calculate_cosine_similarity(topic_df)
			ta.save_topic_model(cos_sims, model, "cos_sim")


	def get_sentiment(self, date_range, option):
		"""
        Calls functions to calculate sentiment ratings or query the sentiment ratings if they exist from active graph, 
        pass it to Sentiment Analyser and save it as CSV file.

        Args:
            date_range (list): All dates in between start and end date including those.
			option (list): Containing strings which indicate the chosen input options to use.
        """
		sa = SentimentAnalyser(self.database)
		file_location = Path().absolute() / "data" / "analysis_results" / "sentiment_analysis.csv"
		read_property = 'cleaned_text'

		for nr in option:
			if nr == '1':
				sentiment_tweet_df = sa.calculate_sentiment_tweet_df(date_range, read_property)
				sa.save_as_csv(sentiment_tweet_df, file_location)
				print("Finished writing Sentiment Analysis into CSV file.")
			elif nr == '2':
				sentiment_tweet_df = sa.calculate_sentiment_tweet_df(date_range, read_property)
				self.writer.write_sentiment_ratings(sentiment_tweet_df)
			elif nr == '3':
				get_user = input("Get average from all Users (u) or only Keyplayer (k): ")
				sentiment_tweet_df = sa.get_sentiment_tweet_df(date_range, get_user)
				sentiment_tweet_df['t.neg_rating'] = sentiment_tweet_df['t.neg_rating'].astype(int)
				sentiment_tweet_df['t.pos_rating'] = sentiment_tweet_df['t.pos_rating'].astype(int)
				sentiment_tweet_df['weighted_neg'] = sentiment_tweet_df['t.neg_rating'] * (1 + sentiment_tweet_df['retweets'].astype(int))
				sentiment_tweet_df['weighted_pos'] = sentiment_tweet_df['t.pos_rating'] * (1 + sentiment_tweet_df['retweets'].astype(int))

				date_rating_list = []

				for date in sentiment_tweet_df.date.unique():
					date_avg_df = sentiment_tweet_df.loc[sentiment_tweet_df['date'] == date]
					mean_pos = date_avg_df['t.pos_rating'].mean()
					mean_neg = date_avg_df['t.neg_rating'].mean()
					weighted_mean_pos = date_avg_df['weighted_pos'].mean()
					weighted_mean_neg = date_avg_df['weighted_neg'].mean()

					date_rating_list.append([date, mean_pos, mean_neg, weighted_mean_pos, weighted_mean_neg])

					print('Averaged sentiment rating for date: ', date)

				date_rating_df = pd.DataFrame(date_rating_list, columns=['date', 'mean_pos', 'mean_neg', 'weighted_mean_pos', 'weighted_mean_neg'])
				
				sa.save_as_csv(date_rating_df, file_location)