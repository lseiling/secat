import time
from TwitterSearch import *
from csv import reader as csv_reader

class TweetEnricher(object):

    def __init__(self, file_path):
        self.get_access_info(file_path)

    def runtime(self, runtime_start):
        """
        Prints the current runtime of the program.

        Args:
            runtime_start (float): Starting time of the program.
        """
        print("[total runtime - %s sec]" % round(time.time() - runtime_start, 2))

    def get_access_info(self, file_path):
        """
        Gets the access data needed to use the official Twitter API.

        Args:
            file_path (str): Path and name of access data for Twitter API.

        Returns:
            list: Access data needed to use Twitter API.
        """
        try:
            auth_dict = {}
            with open(file_path) as csvfile:
                readCSV = csv_reader(csvfile, delimiter='\t')
                for row in readCSV:
                    auth_dict[row[0].strip()] = row[1]
            self.auth_dict = auth_dict

        except:
            raise LookupError("Cannot get access information from '/data/twitter_access.csv'.")

    def twitter_search_connection(self):
        """
        Sets up the access data needed to use the official Twitter API.

        Args:
            self.auth_dict (list): Access data needed to use Twitter API.

        Returns:
            tuple: Access data needed to use Twitter API.
        """
        return TwitterSearch(
                consumer_key = self.auth_dict['consumer_key'],
                consumer_secret = self.auth_dict['consumer_secret'],
                access_token = self.auth_dict['access_token'],
                access_token_secret = self.auth_dict['access_token_secret'])

    def get_twitter_data(self, ts, tuo, get_tweets=False):
        """
        Scrapes Twitter data using the Twitter API.

        Args:
            ts (tuple): Access data needed to use Twitter API.
            tuo (class): Access timelines of users.
            get_tweets (bool): Checks if only Users are to enrich or the Tweets also.

        Returns:
            twitter_data (dict): Containing tweet data to write into active graph.
        """
        twitter_data = {}
        tweet_dict_list = []
        saved_user_data = False

        for tweet in ts.search_tweets_iterable(tuo):
            if not saved_user_data:
                twitter_data['screen_name'] = tweet['user']['screen_name']
                twitter_data['id'] = tweet['user']['id']
                twitter_data['verified'] = tweet['user']['verified']
                twitter_data['followers_count'] = tweet['user']['followers_count']
                twitter_data['friends_count'] = tweet['user']['friends_count']
                saved_user_data = True

            elif saved_user_data and not get_tweets:
                break

            elif get_tweets:  
                tweet_data = {}
                tweet_data['id'] = tweet['id_str']
                tweet_data['text'] = tweet['text']
                tweet_data['source'] = tweet['source']
                tweet_data['retweet_count'] = tweet['retweet_count']
                tweet_data['in_reply_to_screen_name'] = tweet['in_reply_to_screen_name']
                tweet_dict_list.append(tweet_data)

        if get_tweets:
            twitter_data['tweets'] = tweet_dict_list

        return twitter_data

    def get_data_for_users(self, usernames, get_tweets=False):
        """
        Scrapes Twitter data using the Twitter API.

        Args:
            usernames (list): Containing strings of Usernames.
            get_tweets (bool): Checks if only Users are to enrich or the Tweets also.

        Returns:
            twitter_data (dict): Containing tweet data to write into active graph.
        """
        runtime_start = time.time()

        self.user_info = []
        for user in usernames:
            while True:
                print("Enriching User and Tweet Nodes for: {}".format(user))
                try:
                    tuo = TwitterUserOrder(user)
                    ts = self.twitter_search_connection()
                    twitter_data = self.get_twitter_data(ts, tuo, get_tweets=get_tweets)
                    self.user_info.append(twitter_data)
                except TwitterSearchException as e: 
                    print(e)
                    if str(e).startswith('Error 429'):
                        print("Sleeping for 15min because of Twitter API rate limiting. Data extraction will continue after the break.")
                        time.sleep(905)
                        continue
                    break
                break

        self.runtime(runtime_start)