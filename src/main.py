﻿import os
import datetime
import platform
from analysis import *
from data_preprocessing import *

def clear():
    """
    Clears command line.
    """
    if platform.system() == "Windows":
        os.system('cls')
    else:   
        os.system('clear')

def print_heading(heading):
    """
    Prints heading in command line.

    Args:
        heading (str): Text.
    """
    dh.update_node_rel_counts()
    print("""

                    YOU ARE CONNECTED TO A NEO4J GRAPH DATABASE
                    with {} Nodes and {} Relationships
________________________________________________________________________________
                                 {}
________________________________________________________________________________""".format(dh.num_nodes, dh.num_rels, heading))

def check_input(given_input, allowed_input):
    """
    Checks command line user input.

    Args:
        given_input (str): User input.
        allowed_input (list): Contains allowed input options.
    """
    if given_input in allowed_input:
        pass
    else:
        raise ValueError('The provided input is not a valid option.')

################################ SCRAPING MENU ################################
def make_date(date_string):
    """
    Changes date input into needed date format.

    Args:
        date_string (str): Date.

    Returns:
        str: Date in correct format.
    """
    date_info = date_string.split("-")
    if [len(info) for info in date_info] == [4,2,2]:
        return datetime.date(int(date_info[0]),int(date_info[1]),int(date_info[2]))
        raise ValueError('The provided string does not fit the required format.')
    elif [len(info) for info in date_info] == [2,2,4]:
        return datetime.date(int(date_info[2]),int(date_info[1]),int(date_info[0]))
        raise ValueError('The provided string does not fit the required format.')

def scrape():
    """
    Starts scraping menu, prints information in command line, asks for user input and calls further functions from used classes.

    Returns:
        bool: True.
    """
    clear()
    print_heading("SCRAPE DATA")
    print("""
   The data will be scraped and cleaned using the standard cleaning pipeline.
   You can reclean the original text with your custom cleaning pipeline using 
                    the Cleaning Option (4) in the main menu.

        """)

    query_string = input('1. Please provide the Twitter query for which you want to scrape the results: ')
    start_date = make_date(input('2. Please provide the date from which you want to start scraping [format: DD-MM-YYYY]: '))
    end_date =  make_date(input('3. Please provide the date at which you want to stop scraping [format: DD-MM-YYYY]: '))
    tweet_num = int(input('4. Please provide the number of tweets you want to scrape for each day (0 will scrape all available tweets): '))
    print("________________________________________________________________________________\n")

    print("\n######### STARTING SCRAPING PROCESS #########")
    dh.get_and_write_data(query_string, start_date, end_date, tweet_num)
    print("######### FINISHED SCRAPING PROCESS #########\n")

    input('Press ENTER to return to the main menu.')
    return True

################################ LOAD/SAVE MENU ################################
def loadsave():
    """
    Starts dump menu, prints information in command line, asks for user input and calls further functions from used classes.

    Returns:
        connection_to_database (bool): Checks if connection to database is set up.
        switch[option] (list): Chosen user options.
    """
    clear()
    print_heading("LOAD OR SAVE DATA")
    print("""
                    please choose from the following options:

    1) Load data from repository into database
    2) Save data to repository into database

    Exit: q
________________________________________________________________________________

        """)

    connection_to_database = True

    switch = {
        '1': load,
        '2': save
    }
    option = input('Option: ')
    if(option == 'q'):
        return connection_to_database
    else:
        return switch[option]()

def load():
    """
    Starts load data menu, prints information in command line, asks for user input and calls further functions from used classes.

    Returns:
        connection_to_database (bool): Checks if connection to database is set up.
    """
    clear()
    print_heading("LOAD DATA")
    print("To load data, please make sure that you have stopped your graph database.\n This is true if the status of the database is set to 'STOPPED'.\n")
    filename = input("Name of the database dump file that you want to load from the repository's 'data' folder: ")
    print()

    connection_to_database = False
    dumpHandler = DumpHandler()
    try:
        dumpHandler.load_dump(filename)
        print("\nPlease make sure to restart your graph so that the status of the database is 'RUNNING'.")
        input('Press ENTER to return to the main menu.')
    except Exception as e:
        print("\nFailed to load data from graph.")
        print(e)
        input('Press ENTER to return to the main menu.')
    return connection_to_database
    
def save():
    """
    Starts save data menu, prints information in command line, asks for user input and calls further functions from used classes.

    Returns:
        connection_to_database (bool): Checks if connection to database is set up.
    """
    clear()
    print_heading("SAVE DATA")
    print("To save data, please make sure that you have stopped your graph database.\nThis is true if the status of the database is set to 'STOPPED'.\n")
    filename = input("Name of the file that you want to save the database to: ")
    print()

    connection_to_database = False
    dumpHandler = DumpHandler()
    try:
        dumpHandler.save_dump(filename)
        print("\nPlease make sure to restart your graph so that the status of the database is 'RUNNING'.")
        input('Press ENTER to return to the main menu.')
    except Exception as e:
        print("\nFailed to save data to graph.")
        print(e)
        input('Press ENTER to return to the main menu.')
    return connection_to_database

################################ ENRICHING MENU ################################
def enrich():
    """
    Starts enrich menu, prints information in command line, asks for user input and calls further functions from used classes.

    Returns:
        connection_to_database (bool): Checks if connection to database is set up.
    """
    clear()
    print_heading("ENRICH DATA")
    option = input('Enrich User (u) or create duplicate relation (d) between Tweet nodes?: ')
    check_input(option, ['u','U','d','D'])

    if(option=='u' or option=='U'):
        dh.start_twitter_search()
        filename = dh.lookup_csv("keyplayers.csv")

        if filename:
            valid_option = False
            while not valid_option:
                load_key_players = input('Do you want to add key_player property to specified user nodes in database or load a key_player list? (y/n): ')
                try:
                    check_input(load_key_players, ['y','Y','n','N'])
                    valid_option = True
                except Exception as ex:
                    print(ex)
                    print("Please try again...\n")
                    continue
                break
            
            if(load_key_players=='y' or load_key_players=='Y'):
                dh.set_key_players(filename)
                input('Press Enter to continue.')


        dh.start_twitter_search()
        connection_to_database = True
        quit = False

        while not quit:
            valid_option = False
            clear()
            print_heading("ENRICH DATA")
            print("""
   Enriching data will use the official Twitter API to add information about 
   A) the users (user id, user verification, user followers count, user friends count)
   B) their tweets (tweet source, in-reply-to-relation)

                    please choose from the following options:

    1) Enrich specific user 
    2) Enrich all users
    3) Enrich all key players

    Back: Enter
________________________________________________________________________________

            """)
            while not valid_option:
                option = input('Option: ')
                try:
                    check_input(option, ['1','2','3']+['q'])
                    valid_option = True
                except Exception as ex:
                    print(ex)
                    print("Please try again...\n")
                    continue
                break
            if option == 'q':
                quit = True
                break
            
            valid_option = False
            get_tweets = False
            while not valid_option:
                tweet_dec = input("\nPlease indicate if you want to enrich only users (u) or users and their tweets (t): ")
                try:
                    check_input(tweet_dec, ['u','U','t','T'])
                    if tweet_dec in ['t','T']:
                        get_tweets = True
                    valid_option = True
                except Exception as ex:
                    print(ex)
                    print("Please try again...\n")
                    continue
                break

            switch = {
                '1': "single user",
                '2': "all users",
                '3': "key players"
            }
            dh.enrich_data(switch[option], get_tweets)
            input('Press Enter to return to the Enrichment Menu')
        
        return connection_to_database

    elif(option=='d' or option=='D'):
        write_duplicate()

def write_duplicate():
    """
    Calls datahandler function to write IS_DUPLICATE_OF relation between nodes in active graph.
    """
    dh.write_duplicate_rel()

################################ CLEANING MENU ################################
def transform_to_list(input_str):
    """
    Converts given string to list.

    Args:
        input_str (str): User input from command line.

    Returns:
        num_list (list): Converted user input string.
    """
    nums_allowed = [0,1,2,3,4,5,6,7,8]
    num_list = [int(x.strip()) for x in input_str.split(',')]
    if any(x not in nums_allowed for x in num_list):
        raise ValueError('The input contains invalid symbols.')
    else:
        return num_list

def clean():
    """
    Starts cleaning menu, prints information in command line, asks for user input and calls further functions from used classes.

    Returns:
        connection_to_database (bool): Checks if connection to database is set up.
    """
    clear()
    print_heading("CLEAN DATA")
    print("""
            please choose combination and order of cleaning options

    0) tokenize (incl. all options)     4) remove numbers 
    1) remove urls                      5) remove special characters
    2) remove mentions                  6) remove excess white space
    3) remove emojis                    7) lower case
                                        8) remove apostrophes (keeps contractions)

    Combine with ','


    Exit: q
________________________________________________________________________________

        """)
    connection_to_database = True
    cleaning_started = False

    option = input('Option: ')
    if(option != 'q'):
        property_name = input('Into which property would you like to write the data?: ')
        while not cleaning_started:
            try:
                cleaning_order = transform_to_list(option)

                print("\n######### STARTING CLEANING PROCESS #########")
                cleaning_started = True
                dh.get_and_write_clean_texts(cleaning_order, property_name)
                print("######### FINISHED SCRAPING PROCESS #########\n")
                input('Press ENTER to return to the main menu.')

            except Exception as ex:
                print(ex)
                option = input('Please try again: ')
                if option == 'q':
                    break
                continue
            break
    return connection_to_database

################################ ANALYSIS MENU ################################
def analyse():
    """
    Starts analyse menu, prints information in command line, asks for user input and calls further functions from used classes.

    Returns:
        connection_to_database (bool): Checks if connection to database is set up.
    """
    print("\nThe initialisation of the Analysis Menu can take a few seconds...")
    
    connection_to_database = True
    quit = False

    while not quit:
        valid_option = False
        clear()
        print_heading("ANALYSE DATA")
        print("""
                    please choose from the following options:

    1) Calculate basic metrics (tweet frequencies, ...)
    2) Start Sentiment Analysis
    3) Start Topic Modelling


    Exit: q
________________________________________________________________________________

        """)
        while not valid_option:
            option = input('Option: ')
            try:
                check_input(option, ['1','2','3']+['q'])
                valid_option = True
            except Exception as ex:
                print(ex)
                print("Please try again...\n")
                continue
            break
        if option == 'q':
            quit = True
            break

        switch = {
            '1': calc_metrics,
            '2': analyse_sentiment,
            '3': analyse_topics
        }
        switch[option]()
        input('Press Enter to return to the Analysis Menu')
    
    return connection_to_database

def calc_metrics():
    """
    Calls functions to calculate base metrics.
    """
    print("\n######### STARTING BASIC METRICS CALCULATION #########")
    dh.start_analysis()
    dh.get_and_save_base_metrics()

def analyse_sentiment():
    """
    Starts analyse sentiment menu, prints information in command line, asks for user input and calls further functions from used classes.
    """
    clear()
    print_heading("ANALYSE DATA")
    print("""
                    please choose from the options

    1) save sentiment analysis results in csv file
    2) write sentiment analysis results into DB
    3) save sentiment rating averaged per date in csv file

    Combine with ','


    Exit: q
________________________________________________________________________________

        """)
    sentiment_analysis_started = False
    option = input("Option: ")
    if(option != 'q'):
        write_all = input("Sentiment Analysis on all dates in your DB? (y/n): ")
        while not sentiment_analysis_started:
            try:
                option = option.replace(" ", "").split(",")
                sentiment_analysis_started = True

                if(write_all == 'y'):
                    #get daterange from db
                    date_df = dh.get_dates()
                    start_date = make_date(date_df['date'].iloc[0])
                    end_date = make_date(date_df['date'].iloc[-1])
                    date_range = dh.get_daterange(start_date, end_date)
                    dh.get_sentiment(date_range, option)
                else:
                    #user input daterange
                    start_date = make_date(input('Provide start date - [format: DD-MM-YYYY]: '))
                    end_date =  make_date(input('Provide end date - [format: DD-MM-YYYY]: '))
                    date_range = dh.get_daterange(start_date, end_date)
                    dh.get_sentiment(date_range, option)
            except Exception as ex:
                print(ex)
                option = input('Please try again: ')
                if option == 'q':
                    break
                continue
            break

def analyse_topics():
    """
    Starts topic model menu, prints information in command line, asks for user input and calls further functions from used classes.
    """
    clear()
    print_heading("ANALYSE DATA")
    print("""
                    please choose the models to calculate

    LDA) Latent Dirichlet Allocation
    NMF) Non-negative Matrix Factorization

    Combine with ','


    Exit: q
________________________________________________________________________________

        """)
    topic_modeling_started = False

    option = input("Choose the model you want you calculate?: ")
    if(option != 'q'):
        write_all = input("Topic Model on all dates in your DB? (y/n): ")
        while not topic_modeling_started:
            try:
                model_option = option.replace(" ", "").upper().split(",")
                topic_modeling_started = True

                if(write_all == 'y'):
                    #get daterange from db
                    date_df = dh.get_dates()
                    start_date = make_date(date_df['date'].iloc[0])
                    end_date = make_date(date_df['date'].iloc[-1])
                    dh.get_topics(start_date, end_date, model_option)
                else:
                    #user input daterange
                    start_date = make_date(input('Provide start date - [format: DD-MM-YYYY]: '))
                    end_date =  make_date(input('Provide end date - [format: DD-MM-YYYY]: '))
                    dh.get_topics(start_date, end_date, model_option)
            except Exception as ex:
                print(ex)
                option = input('Please try again: ')
                if option == 'q':
                    break
                continue
            break

################################ START AND MAIN MENU ################################
def retry_connection():
    """
    Retry to connect to graph if exeption is thrown.

    Returns:
        response (str): User input.
    """
    print("\nCONNECTION TO DATABASE FAILED.")
    print("Please make sure that the graph database is live.\nThis is true if the status of the database is set to 'RUNNING'.")
    response = input("To try again, press the ENTER key.\nTo quit, type 'q' and ENTER.\n")
    return response

clear()
print("""
################################################################################
                                    SECAT 
 framework for the Scraping, Enrichment, Cleaning and Analysis of Twitter data

               by Vivien Fröhlich, Raphael Chmiel & Lukas Seiling
################################################################################
    """)  

print("Welcome to the Twitcoin Project!\nBefore continuing, please make sure that you have")
print("1. Read the readme.md")
print("2. Set up Neo4j Desktop.")
print("3. Have a graph database running.\n")

RUNNING = True
global INITIALISED 
global CONNECTED
INITIALISED = False
CONNECTED = False

while RUNNING == True:
    

    quit = False
    while(not quit):
        while not INITIALISED:
            try:
                dh = DataHandler()
                INITIALISED = True
                CONNECTED = True
            except Exception as ex:
                response = retry_connection()
                if response == 'q':
                    RUNNING = False
                    CONNECTED = True
                    quit = True
                    break
                continue
            break

        while not CONNECTED:
            try:
                dh.connect_to_database()
                CONNECTED = True
            except Exception as ex:
                response = retry_connection()
                if response == 'q':
                    RUNNING = False
                    quit = True
                    break
                continue
            break

        if quit == True:
            break

        clear()
        print_heading("MAIN MENU")
        print("""
                    please choose from the following options:

    1) Scrape data from Twitter into graph database
    2) Load/save data from/to repository into database
    3) Enrich data in database
    4) Clean data in database
    5) Analyse data in database

    Exit: q
________________________________________________________________________________

        """)
        valid_option = False
        
        while not valid_option:
            option = input('Option: ')
            try:
                check_input(option, ['1','2','3','4','5']+['q'])
                valid_option = True
            except Exception as ex:
                print(ex)
                print("Please try again...\n")
                continue
            break
        
        if(option == 'q'):
            quit = True
            RUNNING = False
        else:
            switch = {
                '1': scrape,
                '2': loadsave,
                '3': enrich,
                '4': clean,
                '5': analyse
            }
            CONNECTED = switch[option]()