from data_preprocessing import TweetCleaner


c = TweetCleaner()

def test_remove_usernames():
    test_string = "Decent idea. My mate @KillerKingKobe knows more than me!"
    res_string = "Decent idea. My mate  knows more than me!"
    assert c.remove_usernames(test_string) == res_string

def test_remove_emojis():
    test_string = "This 🐻 is how Lukas typically uses emojis🐸✌🏼"
    res_string = "This  is how Lukas typically uses emojis"
    assert c.remove_emojis(test_string) == res_string

def test_remove_urls():
    test_string = "#Robostopia Smart #CryptoCurrency Trader Bot many at http://robostopia.com check it outpic.twitter.com/KEJizJNowD"
    res_string = "#Robostopia Smart #CryptoCurrency Trader Bot many at  check it out "
    assert c.remove_urls(test_string) == res_string

def test_remove_numbers():
    test_string = "Bitcoin (BTC) Price: Dips below $6500 – How Bad is That? (Technical Analysis):"
    res_string = "Bitcoin (BTC) Price: Dips below $ – How Bad is That? (Technical Analysis):"
    assert c.remove_numbers(test_string) == res_string

def test_remove_special_chars():
    test_string = "Bitcoin (BTC) Price: Dips below $6500 – How Bad is That? (Technical Analysis):"
    res_string = "Bitcoin BTC Price Dips below 6500  How Bad is That? Technical Analysis"
    assert c.remove_special_chars(test_string) == res_string

def test_remove_excess_whitespace():
    test_string = "    Who    goes there?   "
    res_string = "Who goes there?"
    assert c.remove_excess_white_space(test_string) == res_string

def test_lower_case():
    test_string = "Would be interesting to compare that to r/Bitcoin and r/Monero"
    res_string = "would be interesting to compare that to r/bitcoin and r/monero"
    assert c.lower_case(test_string) == res_string
