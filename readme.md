# SECAT - framework for the Scraping, Enrichment, Cleaning and Analysis of Twitter Data 

This project implements a python framework that scrapes and analyses twitter data.
The analyses run on the results try to relate the output to the cryptocurrency Bitcoin (BTC).

## Setup

#### Python

Clone the project from GitLab

```shell
git clone https://gitlab.com/lseiling/secat.git
```
Make sure you have python installed.

```shell
python --version
```

We will be using the version 3.7 of python. Make sure this is your current version. If not, update it.

#### Requirements

Install, if necessary, requirements by running the following command inside the project directory.

```shell
pip install -r requirements.txt
```

Make sure to have the latest `pip` version. In some cases might be run as `pip3`.

#### Neo4J

Please make sure, you have the Neo4jDesktop client working on your machine. The installation files and guides can be found [here](https://neo4j.com/download/).

#### Running the Project
As soon as you created a Neo4j database and have it running on your machine, you can start the framework from the git directory using

```shell
python src/main.py
```
The project employs a simple command line-based user interface. All further information is displayed there.

## Tests

For the test, we use the pytest package. The documentation can be found [here](https://docs.pytest.org/en/latest/getting-started.html#install-pytest).

To run all tests, use the following command

```shell
pytest . -v -s -p no:warnings
```

To run tests in one file, run

```shell
pytest src/tests/test_file.py -v -s
```

This will run the tests in all the files in the tests folder.
All test files must have the format `test_*.py`

## Git Workflow

The lists of issues pending to be implemented can be found inside the platform, in the GitLab Issues Board.
All issues will be integrated to the code via MergeRequests as follows.

### Start a new issue

1. Make sure you have the last changes.

```shell
git checkout master
git pull
```

2. Create a new branch named after your issue. Using the number of the issue as prefix to link it to the board. Example `10-positive-corpus`

```shell
git checkout -b branch-name
```

3. Implement your code in the branch you created.

4. Commit your changes

```shell
git add .
git commit -m "Short description of what your changes do"
```
5. Push your branch into the git history

```shell
git push origin branch-name
```

### Submit branch to a MergeRequest

To submit your branch to be added to master you must go to the platform and create a new MergeRequest using the `master` branch
as the objective branch.

If your work is still in process, then make sure to add the prefix
`WIP:` to the name of the MergeRequest.

### Accept a MergeRequest

Once a MergeRequest has been properly reviewed, it can be merged into the master branch through the platform. All conflicts must be resolved in the process.

#### Solving conflicts

If a branch is not up to date with the master branch, meaning that there is new work in master that was not when you started working, you must rebase your branch so that it includes all these changes before.

Once you have uploaded your work as mentioned in point 5, do the following.

1. Get all the new history
```shell
git checkout master
git pull origin master
```

2. Move back to your branch
```shell
git checkout branch-name
```

3. Rebase the master branch
```shell
git rebase master
```

Any conflicts must be manually solved and follow the git instructions to finish the rebase.

4. Push your branch with the new changes
```shell
git push origin branch-name -f
```

The `-f` command implies a forced push, this is necessary because we are rewriting the current history of our branch.
